/*
 * Copyright (C) 2022  Emanuele Sorce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * logbook is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import Ubuntu.Components.Pickers 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import Example 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'logbook.emanuelesorce'
    automaticOrientation: true
    anchorToKeyboard: true

    width: units.gu(45)
    height: units.gu(75)

    property bool changed: false
    property bool editing: false

    function populateTextArea() {
        var when = datepicker.date
        textarea.text = Example.get(when);
        changed = false;
    }

    PageStack {
        id: mainStack
        anchors.fill: parent

        Component.onCompleted: {
            mainStack.push(mainPage)
        }
    }

    Page {
        id: aboutPage
        visible: false

        header: PageHeader {
            title: i18n.tr('About')
        }

        Flickable {
            anchors.fill: parent
            contentHeight:  layout.height + units.dp(80)
            contentWidth: parent.width

            Column {
                id: layout

                spacing: units.dp(35)
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                width: parent.width

                Item {
                    height: units.gu(10)
                    width: 1
                }

                Image {
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: width
                    width: Math.min(parent.width/2, parent.height/3)
                    source: Qt.resolvedUrl("../assets/logo.svg")
                }

                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width * 4 / 5
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    text: "LogBook"
                }

                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width * 4 / 5
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    text: i18n.tr("LogBook is an open source diary app for Ubuntu Touch.<br>Community is what makes this app possible, so pull requests, translations, feedback and donations are very appreciated <br>This app stands on the shoulder of various Open Source projects, see source code for licensing details");
                }

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("❤Donate❤")
                    color: UbuntuColors.green
                    width: parent.width * 4 / 5
                    onClicked: Qt.openUrlExternally("https://paypal.me/emanuele42");
                }

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("See source on:") + " Gitlab"
                    width: parent.width * 4 / 5
                    onClicked: Qt.openUrlExternally("https://gitlab.com/tronfortytwo/logbook");
                }

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Report bug or feature request")
                    width: parent.width * 4 / 5
                    onClicked: Qt.openUrlExternally("https://gitlab.com/TronFortyTwo/logbook/-/issues");
                }

                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width * 4 / 5
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    text: "Copyright (C) 2022 Emanuele Sorce (emanuele.sorce@hotmail.com)"
                }
            }
        }
    }

    Page {
        id: mainPage
        visible: false

        anchors.fill: parent

        header: PageHeader {
            id: header
            title: 'Logbook'

            leadingActionBar.actions: [
                Action {
                    iconName: "home"
                    text: "Today"
                    onTriggered: {
                        datepicker.date = new Date();
                        editing = false;
                        changed = false;
                    }
                }
            ]
            trailingActionBar.actions: [
                Action {
                    iconName: "info"
                    text: "About"
                    onTriggered: {
                        mainStack.push(aboutPage)
                    }
                }
            ]
        }

        Flickable {
            anchors.fill: parent
            contentHeight:  columnlayout.height + units.dp(80)
            contentWidth: parent.width

            ColumnLayout {
                id: columnlayout

                spacing: units.gu(2)
                anchors {
                    margins: units.gu(2)
                    top: header.bottom
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }

                RowLayout {
                    Layout.maximumWidth: parent.width
                    Layout.alignment: Qt.AlignHCenter
                    spacing: units.gu(2)
                    Button {
                        Layout.alignment: Qt.AlignHCenter
                        text: i18n.tr("Yesterday")
                        Layout.preferredWidth: Math.min(units.gu(25), root.width * 0.4)
                        iconName: "previous"
                        onClicked: {
                            var d = new Date();
                            d.setFullYear(datepicker.date.getFullYear());
                            d.setMonth(datepicker.date.getMonth());
                            d.setDate(datepicker.date.getDate() - 1);
                            datepicker.date = d;
                        }
                    }

                    Button {
                        Layout.alignment: Qt.AlignHCenter
                        text: i18n.tr("Tomorrow")
                        Layout.preferredWidth: Math.min(units.gu(25), root.width * 0.4)
                        iconName: "next"
                        iconPosition: "right"
                        onClicked: {
                            var d = new Date();
                            d.setFullYear(datepicker.date.getFullYear());
                            d.setMonth(datepicker.date.getMonth());
                            d.setDate(datepicker.date.getDate() + 1);
                            datepicker.date = d;
                        }
                    }
                }

                DatePicker {
                    Layout.alignment: Qt.AlignHCenter
                    id: datepicker

                    StyleHints {
                        highlightBackgroundColor: theme.palette.normal.raised
                        highlightColor: "#000000"
                    }

                    maximum: {
                        var d = new Date();
                        d.setDate(d.getDate() + 1);
                        return d;
                    }
                    minimum: {
                        var d = new Date();
                        d.setFullYear(d.getFullYear() - 100);
                        return d;
                    }

                    onDateChanged: {
                        populateTextArea();
                    }

                    Component.onCompleted: {
                        populateTextArea();
                    }
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    spacing: units.gu(2)
                    Layout.maximumWidth: parent.width

                    Button {
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredWidth: root.width * 0.4
                        text: i18n.tr("Cancel")
                        iconName: "back"
                        visible: editing
                        onClicked: {
                            textarea.text = ""
                            editing = false
                            changed = false
                            populateTextArea()
                        }
                    }

                    Button {
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredWidth: root.width * 0.4
                        text: i18n.tr("Save")
                        iconName: "save"
                        visible: editing && changed
                        color: UbuntuColors.green
                        onClicked: {
                            Example.set(datepicker.date, textarea.text)
                            editing = false
                        }
                    }

                    Button {
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredWidth: root.width * 0.4
                        text: i18n.tr("Edit")
                        iconName: "edit"
                        visible: !editing
                        onClicked: {
                            editing = true
                        }
                    }
                }

                Text {
                    visible: !editing
                    color: Theme.palette.normal.foregroundText

                    Layout.preferredWidth: root.width * 0.8
                    Layout.alignment: Qt.AlignHCenter
                    Layout.fillHeight: true

                    text: textarea.text.length ? textarea.text : i18n.tr('No entry for this day')
                    wrapMode: Text.WordWrap
                }

                TextArea {
                    id: textarea
                    visible: editing

                    Layout.preferredWidth: root.width * 0.8
                    Layout.preferredHeight: units.gu(60)
                    Layout.alignment: Qt.AlignHCenter
                    Layout.fillHeight: true

                    onTextChanged: {
                        changed = true;
                    }
                }
            }
        }
    }
}
